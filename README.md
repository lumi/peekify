Compile with `cargo build --release`.

Then run `target/release/peekify --help`.

Tadah! :3

Could also install using Cargo with ` cargo install peekify --git https://gitlab.com/lumi/peekify.git`.