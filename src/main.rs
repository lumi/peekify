use {
    image::{
        self,
        GenericImage,
        RgbaImage,
        Rgba,
    },
    std::path::PathBuf,
    structopt::StructOpt,
};

const WALL_BRICK_COLOR: Rgba<u8> = Rgba([0x9A, 0x4E, 0x08, 0xFF]);
const WALL_PLAIN_COLOR: Rgba<u8> = Rgba([0x2D, 0x2D, 0x2D, 0xFF]);
const WALL_BRICK_WIDTH: u32 = 9;
const WALL_BRICK_HEIGHT: u32 = 4;
const WALL_SPACING_X: u32 = 1;
const WALL_SPACING_Y: u32 = 1;
const WALL_WIDTH: u32 = 10;
const WALL_HEIGHT: u32 = 20;
const ROW_OFFSET: [u32; 2] = [3, 8];
const WALL_OFFSET_Y: u32 = 6;
const DEFAULT_ROTATION_ANGLE_DEG: f64 = 45.;

#[derive(StructOpt)]
#[structopt(name = "peekify", about = "A command to generate *peek emojis")]
struct Opt {
    /// Input file
    #[structopt(parse(from_os_str))]
    input: PathBuf,
    /// Output file
    #[structopt(parse(from_os_str))]
    output: PathBuf,
    /// Angle (in degrees)
    #[structopt(short = "a", long = "angle")]
    angle: Option<f64>,
    /// Scale factor after rotation
    #[structopt(short = "s", long = "scale", default_value = "1")]
    scale: f64,
    /// X offset after rotation and scaling
    #[structopt(short = "x", long = "xoffset", default_value = "0")]
    offset_x: f64,
    /// Y offset after rotation and scaling
    #[structopt(short = "y", long = "yoffset", default_value = "0")]
    offset_y: f64,
}

fn image_to_peek(img: RgbaImage, rot_deg: f64, offset_x: f64, offset_y: f64, scale: f64) -> RgbaImage {
    let (img_width, img_height) = img.dimensions();
    let result_size = if img_width > img_height { img_width } else { img_height };
    let mut result = RgbaImage::new(result_size, result_size);
    result.copy_from(&img, result_size / 2 - img_width / 2, result_size / 2 - img_height / 2);
    result = transform(result, rot_deg * ::std::f64::consts::PI / 180., offset_x, offset_y, scale);
    let wall = create_wall(result_size / 2, result_size);
    result.copy_from(&wall, result_size / 2 + result_size % 2, 0);
    result
}

fn rotate_point_around_center(x: f64, y: f64, angle: f64) -> (f64, f64) {
    (angle.cos() * x - angle.sin() * y, angle.sin() * x + angle.cos() * y)
}

fn transform(img: RgbaImage, rot: f64, offset_x: f64, offset_y: f64, scale: f64) -> RgbaImage {
    let (img_width, img_height) = img.dimensions();
    let img_half_width = img_width as f64 / 2.;
    let img_half_height = img_height as f64 / 2.;
    RgbaImage::from_fn(img_width, img_height, |x, y| {
        let x_centered = x as f64 - img_half_width;
        let y_centered = y as f64 - img_half_height;
        let x_translated = x_centered - offset_x;
        let y_translated = y_centered - offset_y;
        let x_scaled = x_translated / scale;
        let y_scaled = y_translated / scale;
        let (x_rotated, y_rotated) = rotate_point_around_center(x_scaled, y_scaled, rot);
        let x_new = x_rotated + img_half_width;
        let y_new = y_rotated + img_half_height;
        let x_rounded = x_new.round();
        let y_rounded = y_new.round();
        if x_rounded >= 0. && y_rounded >= 0. {
            let pixel_x = x_rounded as u32;
            let pixel_y = y_rounded as u32;
            if pixel_x < img_width && pixel_y < img_height {
                *img.get_pixel(pixel_x, pixel_y)
            } else {
                Rgba([0, 0, 0, 0])
            }
        }
        else {
            Rgba([0, 0, 0, 0])
        }
    })
}

fn create_wall(width: u32, height: u32) -> RgbaImage {
    let width_ratio = width as f64 / WALL_WIDTH as f64;
    let height_ratio = height as f64 / WALL_HEIGHT as f64;
    let spacing_x = width_ratio * WALL_SPACING_X as f64;
    let spacing_y = height_ratio * WALL_SPACING_Y as f64;
    let spacing = if spacing_x > spacing_y { spacing_y } else { spacing_x };
    let brick_width = WALL_BRICK_WIDTH as f64 * width_ratio;
    let brick_height = WALL_BRICK_HEIGHT as f64 * height_ratio;
    let col_width = brick_width + spacing;
    let row_height = brick_height + spacing;
    RgbaImage::from_fn(width, height, |x, y| {
        let wall_offset_y = height_ratio * WALL_OFFSET_Y as f64;
        let real_y = y as f64 + wall_offset_y;
        let row = (real_y / row_height).floor();
        let row_offset = ROW_OFFSET[(row as usize) % ROW_OFFSET.len()];
        let row_offset_x = width_ratio * row_offset as f64;
        let real_x = x as f64 + row_offset_x;
        let col = (real_x / col_width).floor();
        let row_y = real_y - row * row_height;
        let col_x = real_x - col * col_width;
        if row_y < brick_height && col_x < brick_width {
            WALL_BRICK_COLOR
        }
        else {
            WALL_PLAIN_COLOR
        }
    })
}

fn main() {
    let opt = Opt::from_args();
    let img = image::open(opt.input).unwrap().to_rgba();
    let peek = image_to_peek(img, opt.angle.unwrap_or(DEFAULT_ROTATION_ANGLE_DEG), opt.offset_x, opt.offset_y, opt.scale);
    peek.save(opt.output).unwrap();
}
